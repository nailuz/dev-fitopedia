import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  constructor( ) { }

  ngOnInit() {
    
  }

  realizarLogoff() {
    this.usuario = null;
    localStorage.removeItem("currentUser");
    window.location.reload();
  }
}
