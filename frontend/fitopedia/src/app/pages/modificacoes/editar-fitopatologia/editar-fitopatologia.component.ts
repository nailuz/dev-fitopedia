import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/core/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-fitopatologia',
  templateUrl: './editar-fitopatologia.component.html',
  styleUrls: ['./editar-fitopatologia.component.css']
})
export class EditarFitopatologiaComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;

  constructor(
    private apiService: ApiService,
    private _router: Router
  ) { }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    if(!this.usuario){
      this._router.navigate(['/home']);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
