var mysql = require('../../mysql').pool;

exports.toViewCrop = (req, res, next) => {

    const queryViewCrop         = `SELECT * FROM Culturas WHERE id = ${req.params.id}`;

    const queryViewDiseases     = `SELECT * FROM Doencas WHERE id_cultura = ${req.params.id} AND status = 1`;

    mysql.getConnection((error, conn) => {
        conn.query(queryViewCrop, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };
                if (results[0].status === 0) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Essa cultura encontra-se desativada.'
                    });
                }

                conn.query(queryViewDiseases, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    };
                    if (results1) {
                        
                        let diseasesList = [];

                        results1.forEach(disease => {
                            diseasesList.push({
                                id_doenca:                      disease.id,
                                nome_comum:                     disease.nome_comum,
                                nome_cientifico:                disease.nome_cientifico,
                                tipo:                           disease.tipo
                            });
                        });

                        return res.status(200).json({
                            error:      null,
                            message:    'Cultura encontrada com sucesso.',
                            dados:{
                                id_cultura:     results[0].id,
                                nome:           results[0].nome,
                                descricao:      results[0].descricao,
                                icone:          "http://localhost:3000/"+results[0].icone,
                                doencas:        diseasesList
                            }
                        });

                    } else {
                        return res.status(500).json({
                            error:      null,
                            message:    'Falha na autenticação.'
                        });
                    };

                });
            } else {
                conn.release();
                return res.status(500).json({
                    error:      null,
                    message:    'Falha na autenticação.'
                });
            };
        });
    });
};


exports.toEditCrop = (req, res, next) => {

    if (!req.body.nome) {
        return res.status(401).json({
            error:      null,
            message:    'É necessário informar o nome da cultura.'
        });
    };

    var id_cultura  = req.params.id;
    var nome        = '';
    var descricao   = '';
    var icone       = '';
    var status      =  1;

    if (req.body.nome)                  { nome = req.body.nome };
    if (req.body.descricao)             { descricao = req.body.descricao };
    if (req.file)                       { icone = req.file.path };

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT * FROM Culturas WHERE nome LIKE '${req.body.nome}'`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length > 0) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Cultura já cadastrada.'
                    });
                };

                if (nome == '')         { nome = results[0].nome };
                if (descricao == '')    { descricao = results[0].descricao };
                if (icone == '')        { icone = results[0].icone };

                var queryEditCrop = `UPDATE Culturas
                                        SET nome        = '${nome}',
                                            descricao   = '${descricao}',
                                            icone       = '${icone}',
                                            status      =  ${status}
                                      WHERE id          =  ${id_cultura}`;

                conn.query(queryEditCrop, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } 
                    if (results1) {
                        return res.status(200).json({
                            error:      null,
                            message:    'Cultura atualizada com sucesso.'
                        });
                    } else {
                        return res.status(401).json({
                            error:      null,
                            message:    'Falha de autenticação.'
                        });
                    };
                });
            } else {
                conn.release();
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};


exports.toDisableCrop = (req, res, next) => {

    var queryDisableCrop = `UPDATE Culturas
                               SET status      = 0
                             WHERE id          = ?`;

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT * FROM Culturas WHERE nome = '${req.body.nome}'`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };
                if (results[0].status == 0) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Essa cultura já se encontra desativada.'
                    });
                };
                conn.query(queryDisableCrop, results[0].id, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } 
                    if (results1) {
                        return res.status(200).json({
                            error:      null,
                            message:    'Cultura desativada com sucesso.'
                        });
                    } else {
                        return res.status(500).json({
                            error:      null,
                            message:    'Houve uma falha inesperada.'
                        });
                    };
                });
            } else {
                conn.release();
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};


exports.cropsList = (req, res, next) => {

    var queryCropsList = `SELECT * FROM Culturas WHERE status = 1`;

    mysql.getConnection((error, conn) => {
        conn.query(queryCropsList, (error, results, fields) => {
            conn.release();
            if (error) {
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };

                let CropList = [];

                results.forEach(cultura => {
                    CropList.push({
                        id_cultura:         cultura.id,
                        nome:               cultura.nome,
                        descricao:          cultura.descricao,
                        icone:              "http://localhost:3000/"+cultura.icone
                    });
                });

                return res.status(200).json({
                    error:      null,
                    message:    'Culturas encontrada com sucesso.',
                    dados:      CropList
                });
            } else {
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};

exports.register = (req, res, next) => {

    var queryRegisterCrop = `INSERT INTO 
                                Culturas (nome, descricao, icone, status) 
                                  VALUES ('${req.body.nome}', '${req.body.descricao}', '${req.file.path}', 1)`;

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT 1 FROM Culturas WHERE nome LIKE '${req.body.nome}'`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length > 0) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Cultura já cadastrada.'
                    });
                };

                conn.query(queryRegisterCrop, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error: error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } 
                    if (results1) {
                        return res.status(201).json({
                            error:      null,
                            message:    'Cultura cadastrada com sucesso.'
                        });
                    } else {
                        return res.status(401).json({
                            error:      null,
                            message:    'Falha de autenticação.'
                        });
                    };
                });
            } else {
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};