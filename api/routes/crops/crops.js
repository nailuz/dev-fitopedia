const express = require('express');
const router = express.Router();

const controller = require('../../controllers/crops/crops-controller');
const file = require('../../middleware/file');
const login = require('../../middleware/login');

router.post('/register', login.requiredUser, file.uploadCrops, controller.register);

router.get('/:id', controller.toViewCrop);
router.patch('/:id', login.requiredUser, file.uploadCrops, controller.toEditCrop);
router.delete('/:id', login.requiredUser, controller.toDisableCrop);

router.get('/', controller.cropsList);

module.exports = router;