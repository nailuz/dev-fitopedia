import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrodoencasComponent } from './cadastrodoencas.component';

describe('CadastrodoencasComponent', () => {
  let component: CadastrodoencasComponent;
  let fixture: ComponentFixture<CadastrodoencasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrodoencasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrodoencasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
