import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroculturasComponent } from './cadastroculturas.component';

describe('CadastroculturasComponent', () => {
  let component: CadastroculturasComponent;
  let fixture: ComponentFixture<CadastroculturasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroculturasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroculturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
