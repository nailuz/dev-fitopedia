import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatogenoscanaComponent } from './patogenoscana.component';

describe('PatogenoscanaComponent', () => {
  let component: PatogenoscanaComponent;
  let fixture: ComponentFixture<PatogenoscanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatogenoscanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatogenoscanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
