import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatogenosmilhoComponent } from './patogenosmilho.component';

describe('PatogenosmilhoComponent', () => {
  let component: PatogenosmilhoComponent;
  let fixture: ComponentFixture<PatogenosmilhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatogenosmilhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatogenosmilhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
