import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HomeComponent } from './home/home.component';
import { DataComponent } from './data/data.component';
import { MilhoComponent } from './milho/milho.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { CadastroculturasComponent } from './cadastroculturas/cadastroculturas.component';
import { CadastrodoencasComponent } from './cadastrodoencas/cadastrodoencas.component';
import { PatogenosComponent } from './patogenos/patogenos.component';
import { ResultadobuscasComponent } from './resultadobuscas/resultadobuscas.component';
import { PatogenoscafeComponent } from './patogenoscafe/patogenoscafe.component';
import { PatogenossojaComponent } from './patogenossoja/patogenossoja.component';
import { PatogenosmilhoComponent } from './patogenosmilho/patogenosmilho.component';
import { PatogenoscanaComponent } from './patogenoscana/patogenoscana.component';


@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HomeComponent,
    DataComponent,
    MilhoComponent,
    CadastroComponent,
    CadastroculturasComponent,
    CadastrodoencasComponent,
    PatogenosComponent,
    ResultadobuscasComponent,
    PatogenoscafeComponent,
    PatogenossojaComponent,
    PatogenosmilhoComponent,
    PatogenoscanaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'data',// localhost:4200/data
        component: DataComponent

      },
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'heroes',
       component: HeroesComponent
      },
      {
        path: 'milho',
       component: MilhoComponent
      },
      {
        path: 'cadastro',
       component: CadastroComponent
      },
      
      {
        path: 'cadastroculturas',
       component: CadastroculturasComponent
      },
      {
        path: 'cadastrodoencas',
       component: CadastrodoencasComponent
      },
      {
        path: 'patogenos',
       component: PatogenosComponent
      },
      {
        path: 'resultadobuscas',
       component: ResultadobuscasComponent
      },
      {
        path: 'patogenoscafe',
       component: PatogenoscafeComponent
      },
      {
        path: 'patogenossoja',
       component: PatogenossojaComponent
      },
      {
        path: 'patogenosmilho',
       component: PatogenosmilhoComponent
      },
      {
        path: 'patogenoscana',
       component: PatogenoscanaComponent
      },
    ])

  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
