import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatogenoscafeComponent } from './patogenoscafe.component';

describe('PatogenoscafeComponent', () => {
  let component: PatogenoscafeComponent;
  let fixture: ComponentFixture<PatogenoscafeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatogenoscafeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatogenoscafeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
