import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilhoComponent } from './milho.component';

describe('MilhoComponent', () => {
  let component: MilhoComponent;
  let fixture: ComponentFixture<MilhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
