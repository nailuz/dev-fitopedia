import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatogenossojaComponent } from './patogenossoja.component';

describe('PatogenossojaComponent', () => {
  let component: PatogenossojaComponent;
  let fixture: ComponentFixture<PatogenossojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatogenossojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatogenossojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
