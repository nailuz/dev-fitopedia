import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatogenosComponent } from './patogenos.component';

describe('PatogenosComponent', () => {
  let component: PatogenosComponent;
  let fixture: ComponentFixture<PatogenosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatogenosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatogenosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
