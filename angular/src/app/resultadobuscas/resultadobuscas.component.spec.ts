import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadobuscasComponent } from './resultadobuscas.component';

describe('ResultadobuscasComponent', () => {
  let component: ResultadobuscasComponent;
  let fixture: ComponentFixture<ResultadobuscasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadobuscasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadobuscasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
